#!/usr/bin/python
import requests
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--user', dest='user', default='admin',
                    help='authentication username')
parser.add_argument('--password', dest='password', default='password123',
                    help='authentication password')
parser.add_argument('--url', dest='url', default='https://tech-tests.digitalberry.fr',
                    help='authentication password')

args = parser.parse_args()
login = args.user
password = args.password
url = args.url

# Login
login_request = requests.post(url + '/login/',
                              data={'username': login, 'password': password})
value = login_request.json()
tokenV = value['token']
header = {'Authorization': 'token ' + tokenV}
# Insert Currencies

print('Insert Currencies')
r = requests.post(url + '/currency/',
                  data={'code': 'GBP', 'name': 'Pound sterling', 'symbol': '£'}, headers=header)
print(r.status_code)
if r.status_code == 400:
    print(r.text)
r = requests.post(url + '/currency/',
                  data={'code': 'JPY', 'name': 'Yen Japonais', 'symbol': '¥'}, headers=header)
print(r.status_code)
r = requests.post(url + '/currency/',
                  data={'code': 'USD', 'name': 'Dollar américain', 'symbol': '$'}, headers=header)
print(r.status_code)
r = requests.post(url + '/currency/',
                  data={'code': 'EUR', 'name': 'euro', 'symbol': '€'}, headers=header)
print(r.status_code)

# Insert Exchange Rate

print('Insert Exchange Rate')
r = requests.post(url + '/exchange/',
                  data={'currency1': 'EUR', 'currency2': 'USD', 'value': 1.07}, headers=header)
r = requests.post(url + '/exchange/',
                  data={'currency1': 'USD', 'currency2': 'EUR', 'value': 0.94}, headers=header)
print(r.status_code)
r = requests.post(url + '/exchange/',
                  data={'currency1': 'EUR', 'currency2': 'GBP', 'value': 0.85}, headers=header)
print(r.status_code)
r = requests.post(url + '/exchange/',
                  data={'currency1': 'EUR', 'currency2': 'JPY', 'value': 120.2}, headers=header)
print(r.status_code)
r = requests.post(url + '/exchange/',
                  data={'currency1': 'EUR', 'currency2': 'CAD', 'value': 1.39}, headers=header)
print(r.status_code)
