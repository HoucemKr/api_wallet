from datetime import datetime, timedelta
from django.utils.timezone import utc

from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions
from django.contrib.auth.models import User
from django.contrib.auth.models import Group


class ExpiringTokenAuthentication(TokenAuthentication):
    public_urls = []

    def authenticate_credentials(self, key):
        model = self.get_model()
        try:
            token = model.objects.get(key=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted')

        # This is required for the time comparison
        utc_now = datetime.utcnow()
        utc_now = utc_now.replace(tzinfo=utc)

        # TODO: HZR, Set the token timeout in settings.py
        if token.created < utc_now - timedelta(hours=24):
            raise exceptions.AuthenticationFailed('Invalid token')
        else:
            # HZR: Renew token date
            token.created = datetime.utcnow()
            token.save()

        return token.user, token

    def authenticate(self, request):
        # If the URLs/method is public, use an anonymous user
        for method, url in self.public_urls:
            if request.method == method and request.path == url:
                u = User.objects.get_or_create(username="UnregisterdUser")[0]
                return (u, None)
        # else, force authenticate:
        return super(ExpiringTokenAuthentication, self).authenticate(request)
