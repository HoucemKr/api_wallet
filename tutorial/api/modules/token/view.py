from datetime import datetime, timedelta
from django.utils.timezone import utc

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.decorators import authentication_classes
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework import exceptions, status


class ObtainExpiringAuthToken(ObtainAuthToken):
    throttle_classes = ()
    permission_classes = ()
    authentication_classes = ()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():  # raise_exception=True)
            return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)

        user = serializer.validated_data['user']
        #
        token, created = Token.objects.get_or_create(user=user)
        #
        # This is required for the time comparison
        utc_now = datetime.utcnow()
        # utc_now = utc_now.replace(tzinfo=utc)
        #
        #
        # HZR: If already exists
        if not created:
            # TODO: HZR, Set the token timeout in settings.py
            utc_now = utc_now.replace(tzinfo=token.created.tzinfo)
            if token.created < utc_now - timedelta(hours=24):
                # HZR: Token has expired, generate another one
                Token.delete(token)
                token = Token.objects.create(user=user)
            else:
                # update the created time of the token to keep it valid
                token.created = datetime.utcnow()
                token.save()
        else:
            # If it is a new token, nothing to do
            pass

        result = {'token': token.key, 'id': user.id, 'email': user.email}
        # print(dir(token))
        print(token.created)
        # print(user)

        return Response(result)


class LogoutView(APIView):
    """
    Logout the user and cleanup it's token.

    * Requires token authentication. (Default behaviour)
    """

    def post(self, request, format=None):
        if len(request.data) == 0:
            request.user.auth_token.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

# obtain_expiring_auth_token = ObtainExpiringAuthToken.as_view()
