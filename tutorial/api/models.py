from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


class ExchangeRate(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    currency1 = models.TextField()
    currency2 = models.TextField()
    value = models.FloatField()


class Currency(models.Model):
    code = models.TextField(unique=True)
    name = models.TextField()
    symbol = models.TextField()


