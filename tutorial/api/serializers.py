from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import ExchangeRate, Currency, LANGUAGE_CHOICES, STYLE_CHOICES


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class ExchangeRateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ExchangeRate
        fields = ('time', 'currency1', 'currency2', 'value')


class CurrencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Currency
        fields = ('code', 'name', 'symbol')
