from django.shortcuts import render
from django.contrib.auth.models import User, Group
from .models import ExchangeRate, Currency
from rest_framework import viewsets
from rest_framework import permissions
from tutorial.api.permissions import IsAdminOrReadOnly
from tutorial.api.serializers import UserSerializer, GroupSerializer, ExchangeRateSerializer, CurrencySerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class ExchangeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated, IsAdminOrReadOnly,)
    queryset = ExchangeRate.objects.all()
    serializer_class = ExchangeRateSerializer


class CurrencyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated, IsAdminOrReadOnly,)
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer

# Create your views here.
